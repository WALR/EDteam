# EDteam App Movil
Aplicación no oficial de EDteam.

- Contenido de Youtube
- Descarga de Videos para verlo offline

## Issues(Errores, Feature)
Si encuentras un error o quisieras alguna feature puedes agregarlo [AQUI](https://github.com/WilfredLemus/FaceDetect/issues/new) 

## Contribución
Agradecemos tu aportación, puedes realizar tus aportaciones en la rama ``dev `` :
- Explica muy bien que cambios realizas
- Crea un ``Pull requests```

## Tecnologías Utilizadas
- [Ionic](http://ionicframework.com/) 
- [Api Youtube](https://developers.google.com/youtube/v3/)


## Config & Start
1. Clone el repositorio e ingrese a la carpeta del mismo.

2. Instalar las dependencias:
    - ```npm install -g ionic@latest ```
    - ```npm install ```

3. Correr el servidor ``` ionic serve ``` , puedes [ver la documentación de ionic](http://ionicframework.com/docs/cli/) para más comandos.

## Video
[![EDteam](https://i.ytimg.com/vi/7ytCMizdE68/hqdefault.jpg)](https://www.youtube.com/watch?v=7ytCMizdE68 "EDteam Video")
